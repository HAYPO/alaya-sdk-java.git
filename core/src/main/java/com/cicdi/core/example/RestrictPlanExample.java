package com.cicdi.core.example;

import com.alaya.contracts.ppos.dto.resp.RestrictingPlan;
import com.alaya.crypto.Credentials;
import com.alaya.crypto.WalletUtils;
import com.alaya.utils.Convert;
import com.cicdi.core.util.Common;
import com.cicdi.core.util.RestrictUtil;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 锁仓计划的演示
 *
 * @author haypo
 * @date 2020/12/3
 */
public class RestrictPlanExample {
    /**
     * 演示创建一个锁仓1.1tp，时长1epoch的计划
     *
     * @param args 可选参数
     * @throws Exception 包含io异常和alaya网络返回的异常
     */
    public static void main(String[] args) throws Exception {
        RestrictUtil restrictUtil = new RestrictUtil(Common.NODE_URL);
        System.out.println("请输入密码");
        String password = new Scanner(System.in).nextLine();
        System.out.println("请输入钱包路径");
        String source = new Scanner(System.in).nextLine();
        System.out.println("请输入所藏地址");
        String account = new Scanner(System.in).nextLine();
        Credentials credentials = WalletUtils.loadCredentials(password, source);
        restrictUtil.createRestrictingPlanWithMidGasProvider(credentials,
                account,
                new ArrayList<RestrictingPlan>() {
                    {
                        add(new RestrictingPlan(
                                BigInteger.ONE,
                                Convert.toVon(BigDecimal.valueOf(1.1), Convert.Unit.ATP).toBigInteger()));
                    }
                });
    }
}
