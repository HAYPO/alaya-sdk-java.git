package com.cicdi.core.util;

import com.alaya.tx.gas.GasProvider;

import java.math.BigInteger;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 存储一些常量
 *
 * @author haypo
 * @date 2020/12/1
 */
public class Common {
    /**
     * 官方开放的rpc接口
     */
    public static final String NODE_URL = "https://openapi.alaya.network/rpc";
    /**
     * 设置一个较小gas用量
     */
    public static final GasProvider MID_GAS_PROVIDER = new GasProvider() {
        @Override
        public BigInteger getGasPrice() {
            return BigInteger.valueOf(1000000000);
        }

        @Override
        public BigInteger getGasLimit() {
            return BigInteger.valueOf(150000);
        }
    };
    /**
     * 并发池
     */
    public static ExecutorService executor = new ThreadPoolExecutor(4, 20,
            0, TimeUnit.MILLISECONDS,
            // 使用有界队列，避免OOM
            new ArrayBlockingQueue<>(5000),
            new ThreadPoolExecutor.DiscardPolicy());
    /**
     * 最小委托金额
     */
    public static long minDelegate = 1L;

    /**
     * 最小委托金额
     */
    public static long minRestrictAmount = 1L;
}
