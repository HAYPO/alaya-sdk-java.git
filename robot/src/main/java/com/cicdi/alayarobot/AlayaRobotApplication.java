package com.cicdi.alayarobot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class AlayaRobotApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlayaRobotApplication.class, args);
    }

}
