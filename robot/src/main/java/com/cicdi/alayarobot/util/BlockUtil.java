package com.cicdi.alayarobot.util;

import com.cicdi.alayarobot.config.AlayaProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigInteger;

/**
 * @author haypo
 * @date 2020/12/7
 */
@Component
public class BlockUtil {
    @Autowired
    AlayaProperties alayaProperties;

    public BigInteger getBlockNumber() throws IOException {
        return alayaProperties.getWeb3j().platonBlockNumber().send().getBlockNumber();
    }

    public BigInteger getBlockNumberInTheInterval() throws IOException {
        return getBlockNumber().mod(BigInteger.valueOf(10750));
    }

    /**
     * @return 距离下一个结算周期还剩多少个区块
     * @throws IOException
     */
    public BigInteger getRestBlockNumberInTheInterval() throws IOException {
        return BigInteger.valueOf(10750).subtract(getBlockNumberInTheInterval());
    }
}
