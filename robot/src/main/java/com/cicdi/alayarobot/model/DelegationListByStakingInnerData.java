package com.cicdi.alayarobot.model;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author haypo
 * @date 2020/12/7
 */
@Data
public class DelegationListByStakingInnerData {
    private String delegateAddr;
    private BigDecimal delegateValue;
    private BigDecimal delegateTotalValue;
    private BigDecimal delegateLocked;
    private BigDecimal delegateReleased;
}
