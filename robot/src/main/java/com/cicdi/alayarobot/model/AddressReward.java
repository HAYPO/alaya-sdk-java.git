package com.cicdi.alayarobot.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

/**
 * @author haypo
 * @date 2020/12/7
 */
@Data
@AllArgsConstructor
public class AddressReward {
    private String address;
    private String nodeId;
    private BigInteger stakingNum;
    private BigInteger reward;

    @Override
    public int hashCode() {
        return ("address: " + address + " node id: " + nodeId + " stakingNum:" + stakingNum).hashCode();
    }

    @Override
    public String toString() {
        return "address: " + address + " node id: " + nodeId + " stakingNum:" + stakingNum + " reward: " + reward;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AddressReward) {
            return this.hashCode() == o.hashCode();
        }
        return false;
    }
}
