package com.cicdi.alayarobot.model;

import lombok.Data;

import java.util.List;

/**
 * {"errMsg":"","code":0,"totalCount":2,"displayTotalCount":2,"totalPages":1,"data":[{"delegateAddr":"atp12un9jxg7tlqu6dr0t4m4p4wce8pen3c5dmjh4f","delegateValue":"15349","delegateTotalValue":"20349","delegateLocked":"15349","delegateReleased":"0"},{"delegateAddr":"atp1cdrwkxpakvfvwvpw35hygttymfp493yh4ujmqj","delegateValue":"5000","delegateTotalValue":"20349","delegateLocked":"5000","delegateReleased":"0"}]}
 *
 * @author haypo
 * @date 2020/12/7
 */
@Data
public class DelegationListByStaking {
    private String errMsg;
    private Integer code;
    private Integer totalCount;
    private Integer displayTotalCount;
    private Integer totalPages;
    private List<DelegationListByStakingInnerData> data;
}
