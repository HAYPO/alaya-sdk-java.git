package com.cicdi.alayarobot.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

/**
 * @author haypo
 * @date 2020/12/7
 */
@Data
@AllArgsConstructor
public class DelegationListByStakingParam {
    private String nodeId;
    private BigInteger pageNo;
    private BigInteger pageSize;
    private BigInteger stakingBlockNum;
}
