package com.cicdi.alayarobot.config;

import com.alaya.protocol.Web3j;
import com.cicdi.alayarobot.service.FastHttpService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * springboot的配置
 *
 * @author haypo
 * @date 2020/12/7
 */
@Slf4j
@Component
@ConfigurationProperties(prefix = "alaya")
@Data
public class AlayaProperties {
    private String nodeId;
    private BigDecimal bonusPer;
    private BigInteger pageSize;
    private String nodeUrl;
    private Web3j web3j;
    private List<String> blackList;

    public Web3j getWeb3j() {
        if (web3j == null && nodeUrl != null) {
            web3j = Web3j.build(new FastHttpService(nodeUrl));
        }
        return web3j;
    }
}
